'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

/**
 * @swagger
 * definitions:
 *  Project:
 *      type: object
 *      properties:
 *          id:
 *              type: integer
 *          person_id:
 *              type: integer
 *          name:
 *              type: string
 *          description:
 *              type: string
 *          url:
 *              type: string
 *          license:
 *              type: string
 *          languages:
 *              type: json
 *              json: ["C#", "Python"]
 *      required:
 *          - person_id
 *          - name
 *          - url
 */
class Project extends Model {
  person() {
    return this.belongsTo('App/Models/Person');
  }
}

module.exports = Project;
