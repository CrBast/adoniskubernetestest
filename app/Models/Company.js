'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

/**
 * @swagger
 * definitions:
 *  Company:
 *      type: object
 *      properties:
 *          id:
 *              type: integer
 *          user_id:
 *              type: integer
 *              info: This field cannot be modified by api. It is automatically applied with the id of the user who created it.
 *          name:
 *              type: string
 *          description:
 *              type: string
 *      required:
 *          - name
 *          - user_id
 */
class Company extends Model {
  user() {
    return this.belongsTo('App/Models/User');
  }

  jobOffers() {
    return this.hasMany('App/Models/JobOffer');
  }

  jobHistories() {
    return this.hasMany('App/Models/JobHistory');
  }

  people() {
    return this.belongsToMany('App/Models/Person').pivotModel(
      'App/Models/JobHistory'
    );
  }
}

module.exports = Company;
