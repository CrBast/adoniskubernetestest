'use strict';

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

/**
 * @swagger
 * definitions:
 *  JobHistory:
 *      type: object
 *      properties:
 *          id:
 *              type: integer
 *          person_id:
 *              type: integer
 *          company_id:
 *              type: integer
 *          activity:
 *              type: integer
 *              pourcentage: 0 to 100 %
 *          start:
 *              type: date
 *          end:
 *              type: date
 *      required:
 *          - person_id
 *          - company_id
 *          - start
 */
class JobHistory extends Model {
  person() {
    return this.belongsTo('App/Models/Person');
  }

  company() {
    return this.belongsTo('App/Models/Company');
  }
}

module.exports = JobHistory;
