'use strict';
const Company = use('App/Models/Company');

class CompanyController {
  /**
   * @swagger
   *
   * /companies:
   *  get:
   *      tags:
   *          - Companies
   *      summary: Get companies list
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/Company"
   *          500:
   *              description: Server error
   */
  async index() {
    const company = await Company.query()
      .with('jobOffers')
      .with('jobHistories')
      .fetch();
    return company;
  }

  /**
   * @swagger
   *
   * /companies/{id}:
   *  get:
   *      tags:
   *          - Companies
   *      summary: Get specific company
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/Company"
   *          404:
   *              description: Not found
   */
  async show({ params, response }) {
    try {
      var company = await Company.findOrFail(params.id);
      await company.loadMany(['jobOffers', 'jobHistories']);
      return company;
    } catch (error) {
      return response.status(404).send({ message: error.message });
    }
  }

  /**
   * @swagger
   *
   * /companies:
   *  post:
   *      tags:
   *          - Companies
   *      security:
   *          - JWT: []
   *      summary: Add new company
   *      parameters:
   *          - name: Company
   *            in: body
   *            required: true
   *            schema:
   *              $ref: "#/definitions/Company"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/Company"
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async store({ request, response, auth }) {
    const companyInfo = request.only(['name', 'description']);
    const company = new Company();
    company.user_id = await auth.user.id;
    company.name = companyInfo.name;
    company.description = companyInfo.description;
    try {
      await company.save();
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
    return company;
  }

  /**
   * @swagger
   *
   * /companies/{id}:
   *  put:
   *      tags:
   *          - Companies
   *      security:
   *          - JWT: []
   *      summary: Full/Partial update [Only owner or admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *          - name: company
   *            in: body
   *            required: true
   *            description: Full or partial company object
   *            schema:
   *              $ref: "#/definitions/Company"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/Company"
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async update({ params, request, response }) {
    const companyInfo = request.only(['name', 'description']);
    const company = await Company.find(params.id);
    if (!company) {
      return response.status(404).json({ message: 'Resource not found.' });
    }
    company.name = companyInfo.name == null ? company.name : companyInfo.name;
    company.description =
      companyInfo.description == null
        ? company.description
        : companyInfo.description;
    try {
      await company.save();
    } catch (error) {
      return response.status(500).json({ message: error.message });
    }
    return company;
  }

  /**
   * @swagger
   *
   * /companies/{id}:
   *  delete:
   *      tags:
   *          - Companies
   *      security:
   *          - JWT: []
   *      summary: Delete company [Only owner or admin]
   *      parameters:
   *          - name: id
   *            in: path
   *            required: true
   *      responses:
   *          200:
   *              description: Success
   *          404:
   *              description: Not found
   *          500:
   *              description: Internal server error
   *          401:
   *              description: Unauthorized
   */
  async destroy({ params, response }) {
    try {
      const company = await Company.find(params.id);
      if (!company) {
        return response.status(404).send({ message: 'Resource not found.' });
      }
      await company.delete();
      return response.status(200).send({ message: 'Success.' });
    } catch (error) {
      return response.status(500).send({ message: error.message });
    }
  }
}

module.exports = CompanyController;
