'use strict';

class AuthController {
  /**
   * @swagger
   *
   * /auth:
   *  post:
   *      tags:
   *          - Auth
   *      summary: Auth session login
   *      parameters:
   *          - name: user
   *            in: body
   *            required: true
   *            schema:
   *              $ref: "#/definitions/User"
   *      responses:
   *          200:
   *              description: Success
   *              schema:
   *                  $ref: "#/definitions/User"
   *          401:
   *              description: Unauthorized => Cannot find user | wrong password
   *
   */
  async login({ auth, response, request }) {
    const data = request.only(['email', 'password']);
    try {
      await auth.check();
      return response.status(200).send({ message: 'You are already logged!' });
    } catch (error) {
      try {
        return await auth.attempt(data.email, data.password);
      } catch (error) {
        return response.status(401).send({ message: 'Bad credentials' });
      }
    }
  }
}

module.exports = AuthController;
