'use strict';

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Swagger Information
  | Please use Swagger 2 Spesification Docs
  | https://swagger.io/docs/specification/2-0/basic-structure/
  |--------------------------------------------------------------------------
  */

  enable: true,
  specUrl: '/swagger.json',
  host: 'crbast.ch/api',

  options: {
    swaggerDefinition: {
      info: {
        title: 'crbast.ch 🚀 REST API',
        version: 'In development'
      },

      schemes: ['https', 'http'],

      contact: {
        email: 'hello@crbast.ch'
      },

      securitySchemes: {
        JWT: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      },

      basePath: '/api',

      // Example security definitions.
      securityDefinitions: {
        JWT: {
          type: 'apiKey',
          in: 'header',
          name: 'Authorization',
          description: 'Add `Bearer` before paste token (Bearer <your-token>).'
        }
      }
    },

    // Path to the API docs
    // Sample usage
    // apis: [
    //    'docs/**/*.yml',    // load recursive all .yml file in docs directory
    //    'docs/**/*.js',     // load recursive all .js file in docs directory
    // ]
    apis: ['app/**/*.js', 'start/routes.js']
  }
};
