'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class NewsSchema extends Schema {
  up() {
    this.create('news', table => {
      table.increments();
      table.string('title', 25).notNullable();
      table.string('details', 255).notNullable();
      table.string('contact', 255).nullable();
      table.string('url', 255).nullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('news');
  }
}

module.exports = NewsSchema;
