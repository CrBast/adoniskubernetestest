'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class JobHistorySchema extends Schema {
  up() {
    this.create('job_histories', table => {
      table.increments();
      table
        .integer('person_id')
        .unsigned()
        .references('id')
        .inTable('people');
      table
        .integer('company_id')
        .unsigned()
        .references('id')
        .inTable('companies');
      table.integer('activity').nullable();
      table.date('start').notNullable();
      table.date('end').nullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('job_histories');
  }
}

module.exports = JobHistorySchema;
