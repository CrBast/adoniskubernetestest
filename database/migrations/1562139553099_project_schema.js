'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ProjectSchema extends Schema {
  up() {
    this.create('projects', table => {
      table.increments();
      table
        .integer('person_id')
        .unsigned()
        .references('id')
        .inTable('people');
      table.string('name', 100).notNullable();
      table.string('description', 255).nullable();
      table.string('url', 255).notNullable();
      table.string('license', 50).nullable();
      table.json('languages').nullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('projects');
  }
}

module.exports = ProjectSchema;
