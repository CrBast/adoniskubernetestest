'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class PersonSchema extends Schema {
  up() {
    this.create('people', table => {
      table.increments();
      table.string('firstname', 80).notNullable();
      table.string('lastname', 80).notNullable();
      table
        .string('email', 100)
        .notNullable()
        .unique();
      table.string('location', 30).notNullable();
      table.json('skils').nullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('people');
  }
}

module.exports = PersonSchema;
