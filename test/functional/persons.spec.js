'use strict';

const { test, trait } = use('Test/Suite')('Persons');

const Person = use('App/Models/Person');
const User = use('App/Models/User');

trait('Test/ApiClient');
trait('Auth/Client');
trait('Session/Client');

test('Persons get list', async ({ client, assert }) => {
  const response = await client.get('api/persons').end();
  response.assertStatus(200);
  assert.isNotNull(response.body);
  var person = response.body[0];
  const dbPerson = await Person.findOrFail(person.id);
  assert.equal(person.firstname, dbPerson.firstname);
  assert.equal(person.lastname, dbPerson.lastname);
  assert.equal(person.email, dbPerson.email);
});

test('Persons get specific person', async ({ client, assert }) => {
  const response = await client.get('api/persons/1').end();
  response.assertStatus(200);
  var person = response.body;
  assert.isNotNull(person);
  const dbPerson = await Person.find(person.id);
  assert.equal(person.firstname, dbPerson.firstname);
  assert.equal(person.lastname, dbPerson.lastname);
  assert.equal(person.email, dbPerson.email);
});

test('Persons add new person', async ({ client, assert }) => {
  const user = await User.findBy('email', 'hello@crbast.ch');
  var person = {
    firstname: 'Test1',
    lastname: 'Test1',
    email: 'test1@test.com',
    location: 'Switzerland'
  };
  const response = await client
    .post('api/persons')
    .loginVia(user, 'jwt')
    .query(person)
    .end();
  response.assertStatus(200);
  var person2 = response.body;
  assert.isNotNull(person2.id);
  assert.equal(person2.firstname, 'Test1');
  assert.equal(person2.lastname, 'Test1');
  assert.equal(person2.email, 'test1@test.com');
  const personToDelete = await Person.findOrFail(person2.id);
  personToDelete.delete();
});

test('Persons modify person', async ({ client, assert }) => {
  const user = await User.findBy('email', 'hello@crbast.ch');
  var person = {
    firstname: 'Test1',
    lastname: 'Test1',
    email: 'test1@test.com',
    location: 'Switzerland'
  };
  var response = await client
    .post('api/persons')
    .loginVia(user, 'jwt')
    .query(person)
    .end();
  response.assertStatus(200);
  var person2 = response.body;
  assert.isNotNull(person2.id);
  assert.equal(person2.firstname, 'Test1');
  assert.equal(person2.lastname, 'Test1');
  assert.equal(person2.email, 'test1@test.com');

  const dbPerson = await Person.findOrFail(person2.id);
  var newPerson = {
    firstname: 'Test2',
    lastname: 'Test2',
    email: 'test2@test.com'
  };
  var response2 = await client
    .put(`api/persons/${dbPerson.id}`)
    .loginVia(user, 'jwt')
    .query(newPerson)
    .end();
  response2.assertStatus(200);
  var newPerson2 = response2.body;
  assert.isNotNull(person2.id);
  assert.equal(newPerson2.firstname, 'Test2');
  assert.equal(newPerson2.lastname, 'Test2');
  assert.equal(newPerson2.location, dbPerson.location);
  assert.equal(newPerson2.email, 'test2@test.com');
  dbPerson.delete();
});

test('Persons delete person', async ({ client, assert }) => {
  const user = await User.findBy('email', 'hello@crbast.ch');
  var person = {
    firstname: 'Test1',
    lastname: 'Test1',
    email: 'test1@test.com',
    location: 'Switzerland'
  };
  var response = await client
    .post('api/persons')
    .loginVia(user, 'jwt')
    .query(person)
    .end();
  response.assertStatus(200);
  var person2 = response.body;
  assert.isNotNull(person2.id);
  assert.equal(person2.firstname, 'Test1');
  assert.equal(person2.lastname, 'Test1');
  assert.equal(person2.email, 'test1@test.com');

  const dbPerson = await Person.findOrFail(person2.id);
  var response2 = await client
    .delete(`api/persons/${dbPerson.id}`)
    .loginVia(user, 'jwt')
    .end();
  response2.assertStatus(200);
  var emptyPerson = await Person.find(dbPerson.id);
  assert.isNull(emptyPerson, 'Person are not deleted');
  var response3 = await client.get(`api/persons/${dbPerson.id}`).end();
  response3.assertStatus(404);
});
