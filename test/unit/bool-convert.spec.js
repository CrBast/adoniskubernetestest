'use strict';

const { boolConverter } = use('App/Helpers/BoolConvert');

const { test } = use('Test/Suite')('Helpers : Bool Convert');

test('boolConverter() int input', async ({ assert }) => {
  assert.isTrue(boolConverter(1));
  assert.isFalse(boolConverter(0));
});

test('boolConverter() int bad input', async ({ assert }) => {
  assert.isNull(boolConverter(34234235));
});

test('boolConverter() string input', async ({ assert }) => {
  assert.isTrue(boolConverter('true'));
  assert.isFalse(boolConverter('false'));
  assert.isTrue(boolConverter('true'));
  assert.isFalse(boolConverter('false'));
});

test('boolConverter() string bad input', async ({ assert }) => {
  assert.isNull(boolConverter('ghjgjh'));
  assert.isNull(boolConverter(null));
});

test('boolConverter() boolean input', async ({ assert }) => {
  assert.isTrue(boolConverter(true));
  assert.isFalse(boolConverter(false));
});
