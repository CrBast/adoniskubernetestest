FROM alpine:latest

# Create app directory
WORKDIR /src

# Disable .env file loading
ENV ENV_SILENT=true

# Set environment variables
ENV NODE_ENV=production
ENV PORT=5000
ENV HOST=0.0.0.0
ENV APP_KEY=

RUN apk add git
RUN apk add yarn

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./
COPY yarn.lock ./

RUN yarn install --production=true

# Bundle app source
COPY . .

# Use non-root user
USER node

EXPOSE 5000
CMD [ "node", "server.js" ]
